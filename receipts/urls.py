from django.urls import path
from receipts.views import (
    category_list,
    create_category,
    create_receipt,
    receipt_list,
    account_list,
    create_account,
)


urlpatterns = [
    path("", receipt_list, name="home"),
    path("categories/", category_list, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
