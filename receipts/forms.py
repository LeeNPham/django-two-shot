from sre_constants import CATEGORY
from django import forms
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        exclude = ["purchaser"]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        exclude = ["owner"]


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = ["owner"]
