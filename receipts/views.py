from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipt_list(request):
    user_receipts = request.user.receipts.all()
    context = {"receipts": Receipt.objects.all, "user_receipts": user_receipts}
    return render(request, "receipts/receipts_list.html", context)

    def get_queryset(self):
        return Receipt.objects.filter(owner=self.request.user)


@login_required
def create_receipt(request):
    form = ReceiptForm(request.POST or None)
    if form.is_valid():
        receipt = form.save(commit=False)
        receipt.purchaser = request.user
        receipt.save()
        return redirect("home")

    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    user_categories = request.user.categories.all()
    context = {
        "categories": ExpenseCategory.objects.all,
        "user_categories": user_categories,
    }
    return render(request, "receipts/categories_list.html", context)

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


@login_required
def account_list(request):
    user_accounts = request.user.accounts.all()
    context = {
        "accounts": Account.objects.all,
        "user_accounts": user_accounts,
    }
    return render(request, "receipts/accounts_list.html", context)

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


@login_required
def create_category(request):
    form = CategoryForm(request.POST or None)
    if form.is_valid():
        category = form.save(commit=False)
        category.owner = request.user
        category.save()
        return redirect("category_list")

    context = {"form": form}
    return render(request, "receipts/create_category.html", context)

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


@login_required
def create_account(request):
    form = AccountForm(request.POST or None)
    if form.is_valid():
        account = form.save(commit=False)
        account.owner = request.user
        account.save()
        return redirect("account_list")

    context = {"form": form}
    return render(request, "receipts/create_account.html", context)

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
