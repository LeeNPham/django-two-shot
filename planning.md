* [x] This feature is about creating a list view for the Receipt model,

* [x] registering the view for a path, 

* [x] registering the receipts paths with the expenses project, 

* [x] and creating a template for the view.

* [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login".

* [x] Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/".

* [x] Create a templates directory under accounts.

* [x] Create a registration directory under templates.

* [x] Create an HTML template named login.html in the registration directory.

* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).

* [x] In the expenses settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home", which will redirect us to the path (not yet created) with the name "home".


* [x] Moving onto Feature 8!
 




















* [x] Protect the list view for the Receipt model so that only a person who has logged in can access it.

* [x] Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user.













Feature 10
* [x] You need to create a __function view__ to handle showing the sign-up form, and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.

* [x] You'll need to import the UserCreationForm from the built-in auth forms 

* [x] You'll need to use the special create_user  method to create a new user account from their username and password

* [x] You'll need to use the login  function that logs an account in

* [x] After you have created the user, redirect the browser to the path registered with the name "home"

* [x] Create an HTML template named signup.html in the registration directory

* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)

* [x] done

* [x] Feature 11

* [x] Create a create view for the Receipt model that will show the vendor, total, tax, date, category and account properties in the form and handle the form submission to create a new Receipt

* [x] A person must be logged in to see the view

* [x] Register that view for the path "create/" in the receipts urls.py and the name "create_receipt"

* [x] Create an HTML template that shows the form to create a new Receipt (see the template specifications below)


